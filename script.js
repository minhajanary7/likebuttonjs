let likeCount = 0;
const likeButton = document.getElementById('likeButton');
const likeCountDisplay = document.getElementById('likeCount');

likeButton.addEventListener('click', () => {
  likeCount++;
  likeCountDisplay.textContent = likeCount;
});
